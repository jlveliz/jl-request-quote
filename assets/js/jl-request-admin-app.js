document.onreadystatechange = () => {
  if (document.readyState == "complete") {
    //Notifications
    let checkNotificateCustomer = document.querySelector(
      "#notificate_customer_for_jl_request_quote"
    );

    checkNotificateCustomer.addEventListener("change", (e) => {
      let checked = e.target.checked;

      if (checked) {
        toggleItemsForNotifications(false);
      } else {
        toggleItemsForNotifications(true);
      }
    });

    //Schedule
    let checkScheduleCustomer = document.querySelector(
      "#schedule_customer_for_jl_request_quote"
    );
    checkScheduleCustomer.addEventListener("change", (e) => {
      let checked = e.target.checked;

      if (checked) {
        toggleItemsForSchedule(false);
      } else {
        toggleItemsForSchedule(true);
      }
    });
  }
};
//Notifications
var toggleItemsForNotifications = (state) => {
  let notificateCustomeJlRequestQuoteBody = document.querySelector(
    "#notificate_customer_for_jl_request_quote_body"
  );
  notificateCustomeJlRequestQuoteBody.disabled = state;

  let emptyTextArea = false;
  if (state) emptyTextArea = true;
  if (emptyTextArea) notificateCustomeJlRequestQuoteBody.value = "";

  let notificateAdminJlRequestQuote = document.querySelector(
    "#notificate_admin_for_jl_request_quote"
  );
  notificateAdminJlRequestQuote.disabled = state;

  let checked = true;
  if (state) checked = false;

  notificateAdminJlRequestQuote.checked = checked;
};

//Schedule

var toggleItemsForSchedule = (state) => {
  let notificateCustomeJlRequestQuoteBody = document.querySelector(
    "#schedule_opening_hour_customer_for_jl_request_quote"
  );

  let startHour = document.querySelector(
    "#schedule_opening_hour_start_customer_for_jl_request_quote"
  );


  let endHour = document.querySelector(
    "#schedule_opening_hour_end_customer_for_jl_request_quote"
  );

  notificateCustomeJlRequestQuoteBody.disabled = state;
  startHour.disabled = state;
  endHour.disabled = state;
};
