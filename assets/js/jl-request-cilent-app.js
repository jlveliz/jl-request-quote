/**
 * Buttons
 */

const btnSendRequestEmail = document.getElementById(
  "request-quote-send-by-email"
);

/**
 * Frm
 */
const formSendRequestQuote = document.getElementById("request-quote-form");

/**
 *
 * Show Form to send email
 *
 */

//btn
btnSendRequestEmail.addEventListener("click", (e) => {
  if (formSendRequestQuote.classList.contains("request-quote-form-hide")) {
    e.target.textContent = "Cancelar";
    formSendRequestQuote.classList.remove("request-quote-form-hide");
    formSendRequestQuote.classList.add("request-quote-form-show");
  } else {
    e.target.textContent = "Enviar por correo";
    formSendRequestQuote.classList.add("request-quote-form-hide");
    formSendRequestQuote.classList.remove("request-quote-form-show");
  }
});

const element = document.getElementById("request-quote-select-form");

var choices = new Choices(element, {
  itemSelectText: "Presione para seleccionar",
  noResultsText: "Sin resultados",
});

// Disable item choice on <select>
element.addEventListener("choice", (e) => {
  if (!e.detail.choice.disabled) {
    e.detail.choice.disabled = true;
    insertDataOnTableRequesQuote(e.detail.choice);
  }
});

document.addEventListener("click", removeItemFromTable, true);

function insertDataOnTableRequesQuote(item) {
  let tableBodyDom = document.querySelector("#request-quote-table tbody");

  // si es la etiqueta
  if (item.placeholder) return false;

  let rowItem = document.createElement("tr");

  let [name, price] = item.value.split("separator");

  // item remove Icon
  let htmlIcon = ` <td> <button type='button' data-item='${item.value}' class='request-quote-table-remove-item'> x </button>  `;
  let htmlItemName = ` <td> ${name}</td>`;
  let htmlItemPrice = ` <td>$ ${price}</td>`;
  let finalHtml = htmlIcon + htmlItemName + htmlItemPrice;

  rowItem.innerHTML = finalHtml;
  tableBodyDom.appendChild(rowItem);

  calculateTotal(price);

  manageItemsOnForm(item);
}

function calculateTotal(price, operation = "add") {
  let totalDom = document.querySelector("#request-quote-total") || 0;
  let totalPrice = totalDom.textContent.trim().replace("$", "") || 0;
  if (operation == "add") {
    totalPrice = parseFloat(totalPrice) + parseFloat(price);
  } else {
    totalPrice = parseFloat(totalPrice) - parseFloat(price);
  }
  totalDom.textContent = "$ " + totalPrice.toFixed(2);
}

function removeItemFromTable(event) {
  // TODO
  let target = event.target;

  let selector = document.querySelectorAll(".request-quote-table-remove-item");

  if (target.classList.contains("request-quote-table-remove-item")) {
    let itemData = target.dataset.item;
    let row = target.closest("tr");
    let price = itemData.split("separator")[1];
    calculateTotal(price, "substract");
    row.remove();
    //Enable Data Item on Choice
    enableChoiceItem(itemData);

    manageItemsOnForm(itemData, "remove");
  }
}

function enableChoiceItem(item) {
  let choice = {};

  choices._currentState.choices.forEach((choiceItem) => {
    if (choiceItem.value == item) {
      choice = choiceItem;
    }
  });

  if (Object.entries(choice).length > 0) {
    choice.disabled = false;

    let domItem = document.getElementById(choice.elementId);
    if (domItem) {
      domItem.classList.replace(
        "choices__item--disabled",
        "choices__item--selectable"
      );
    }
  }
}

function manageItemsOnForm(item, action = "add") {
  let existingItems = document.querySelectorAll(".request-quote-item");
  let [name, price] =
    action == "add" ? item.value.split("separator") : item.split("separator");
  let valueItem = {
    name,
    price,
  };

  if (action == "add") {
    if (btnSendRequestEmail.hasAttribute("disabled")) {
      btnSendRequestEmail.removeAttribute("disabled");
    }

    let domForm = document.querySelector("#request-quote-form");
    let lastItem = existingItems.length > 0 ? existingItems.length : 0;
    let inputFormDom = document.createElement("input");
    inputFormDom.type = "hidden";
    inputFormDom.classList.add("request-quote-item");
    inputFormDom.name = `request_quote_hidden_item[${lastItem}]`;
    inputFormDom.value = JSON.stringify(valueItem);
    domForm.appendChild(inputFormDom);
  } else if (action == "remove") {
    
    let idxArrExistingItems = 0;
    let arrayExistingItems = Array.from(existingItems);
    for (let index = 0; index < arrayExistingItems.length; index++) {
      if (arrayExistingItems[index].value == JSON.stringify(valueItem)) {
        existingItems[index].remove()
        idxArrExistingItems = index;
      }
    }
    arrayExistingItems.splice(idxArrExistingItems, 1);
    if (!arrayExistingItems.length) btnSendRequestEmail.setAttribute("disabled", "disabled");
  }
}

/**
 * =============== end send quote by mail =====================
 */
