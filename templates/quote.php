<?php
$total = 0;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Cotización</title>
    <style>
        @page {
            padding-top: 0.2em;
            padding-bottom: 0.2em;
            size: us-letter;
            margin: 0.5in;
        }

        table {
            width: 100%;
            color: #2b2b2b;
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td,
        table th {
            border: 1px solid #cdcdcd;
        }

        table th {
            text-align: center;
        }

        table .price {
            text-align: right;
        }

        table tfoot td {
            font-size: 14pt;
            font-weight: bold;
        }

        #title {
            text-align: center;
            text-transform: uppercase;
            font-size: 16pt;
        }

        .index-item{
            text-align: center;
        }
    </style>
</head>

<body>

    <div>
        <?php if (has_custom_logo()) : ?>
            
            <?php 
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            ?>
            <img src="<?php echo $image[0]; ?>">
        <?php else : ?>
            <h1><?php bloginfo('name');  ?></h1>
        <?php endif; ?>

        <h2 id="title">Cotización</h2>
    </div>

    <div id="request-body">
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th> Item </th>
                    <th> Precio </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $key => $item) : ?>
                    <tr>
                        <td width="5%" class="index-item"><?php echo $key + 1 ?></td>
                        <td width="80%"><?php echo $item['name'] ?></td>
                        <td class="price" width="15%"><?php echo '$ ' . $item['price'] ?></td>
                        <?php $price += $item['price']; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">Total</td>
                    <td class="price"><?php echo '$ ' .  number_format($price, 2)  ?></td>
                </tr>
            </tfoot>
        </table>
    </div>

</body>

</html>