<?php

//DomPDF
include_once('dompdf/autoload.inc.php');

use Dompdf\Dompdf;

abstract class JlBaseRequest
{
    public $idPplugin = 'request_quote';

    public $domainName = 'jl-request-quote';

    public $subjectCustomer = "Cotización";


    protected function validate_email($string)
    {
        if (!filter_var($string, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    private function get_customer_headers()
    {
        $blog_name = get_option('blogname');
        $headers = [
            'From:' => $blog_name,
        ];
        return $headers;
    }

    protected function send_email_customer($email, $data)
    {
        $html = [];

        foreach ($data as $key => $item) {
            $item_form = json_decode(stripslashes($item));
            $html[] = ['name' =>  $item_form->name, 'price' => $item_form->price];
        }

       
        $attach = $this->generate_pdf($html);
        $message = get_option('notificate_customer_for_jl_request_quote_body');
        add_filter('wp_mail_from_name', [$this, 'replate_from_name_email']);

        $send = wp_mail($email, $this->subjectCustomer, $message, '', $attach);

        if ($send) {
            return true;
        } else {
            return false;
        }
    }


    protected function generate_pdf($data)
    {
        $filename = 'quote.pdf';
        $filename = sanitize_file_name($filename);
        $report_template =  dirname(__DIR__) . "/templates/quote.php";
        $report_path_pdf =  dirname(__DIR__) . '/templates/quote.pdf';

        
        if (file_exists($report_template)) {
            
            extract($data);
            
            
            ob_start();

            include($report_template);

            $report_template = ob_get_contents();

            ob_end_clean();

            $dom_pdf = new Dompdf();
            var_dump($dom_pdf);
            die();
            $dom_pdf->loadHtml($report_template);
            $dom_pdf->render();
            $pdf = $dom_pdf->output();
            file_put_contents($report_path_pdf, $pdf);
            return $report_path_pdf;
        }
    }

    public function replate_from_name_email($original_name)
    {
        return get_option('blogname');
    }


    public function send_email_admin($customer_email, $data)
    {
        $admin_email = get_option('admin_email');

        if ($admin_email != '' && $customer_email) {

            $ip = $_SERVER['HTTP_CLIENT_IP'] ? $_SERVER['HTTP_CLIENT_IP'] : ($_SERVER['HTTP_X_FORWARDED_FOR'] ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);

            $message = " 
            <p>El usuario con correo {$customer_email} 
            con IP : {$ip},  ha solicitado los siguientes Productos</p>
            ";

            $message .= "
                <ul>
            ";

            foreach ($data as $key => $item) {
                $item_form = json_decode(stripslashes($item));
                $message .=  "<li>{$item_form->name} -  $ {$item_form->price}</li>";
            }
            $message .= "
                <ul>
            ";

            $headers = array('Content-Type: text/html; charset=UTF-8');

            return wp_mail($admin_email, "Notificación de ". $this->subjectCustomer, $message, $headers);
        }
    }
}
