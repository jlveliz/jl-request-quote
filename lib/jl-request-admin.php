<?php

require_once 'jl-base-request.php';

class JlRequestAdmin extends JlBaseRequest
{
    public function __construct()
    {
        add_action('init', [$this, 'register_custom_post_request_quote']);
        add_action('init', [$this, 'register_taxonomies_for_request_quote']);
        add_action('save_post', [$this, 'save_custom_post_request_quote'], 10, 2);
        add_filter('manage_' . $this->idPplugin . '_posts_columns', [$this, 'add_column_in_achieve_custom_post_request_quote'], 10, 1);
        add_action('manage_' . $this->idPplugin . '_posts_custom_column', [$this, 'add_data_to_column_in_achieve_custom_post_request_quote'], 10, 2);
        add_action('admin_menu', [$this, 'add_submenu_page_post_request_quote']);
        add_action('admin_head', [$this, 'hidde_button_preview_request_post_page']);
        add_filter('post_row_actions', [$this, 'remove_row_action_on_preview_request_post'], 10, 1);
    }

    public function register_taxonomies_for_request_quote()
    {
        $labels = [
            'name'              => _x('Categorías', $this->domainName),
            'singular_name'     => _x('Categoría', $this->domainName),
            'search_items'      => __('Buscar Categorías'),
            'all_items'         => __('Todas las Categorias'),
            'parent_item'       => __('Categoria Padre'),
            'parent_item_colon' => __('Categoria Padre:'),
            'edit_item'         => __('Editar Categoria'),
            'update_item'       => __('Actualizar Categoria'),
            'add_new_item'      => __('Agregar nueva Categoria'),
            'new_item_name'     => __('Nueva Categoria'),
            'menu_name'         => __('Categorías'),
        ];
        $args = [
            'hierarchical'      => true, // make it hierarchical (like categories)
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => ['slug' => 'course'],
        ];
        register_taxonomy('request_quote_categories', ['request_quote'], $args);
    }

    public function register_custom_post_request_quote()
    {
        $labels = array(
            'name'               => _x('Productos', 'post type general name', $this->domainName),
            'singular_name'      => _x('Producto', 'post type singular name', $this->domainName),
            'menu_name'          => _x('Cotizador', 'admin menu', $this->domainName),
            'add_new'            => _x('Añadir Producto', 'producto', $this->domainName),
            'add_new_item'       => __('Añadir nuevo producto', $this->domainName),
            'new_item'           => __('Nuevo producto', $this->domainName),
            'edit_item'          => __('Editar producto', $this->domainName),
            'all_items'          => __('Productos', $this->domainName),
            'search_items'       => __('Buscar Productos', $this->domainName),
            'not_found'          => __('No hay productos.', $this->domainName),
            'not_found_in_trash' => __('No hay productos en la papelera.', $this->domainName)
        );

        $args = [
            'labels' => $labels,
            'description' => 'Realiza cotizaciones en linea enviar por correo los productos seleccionados',
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'menu_icon' => 'dashicons-format-aside',
            'supports' => ['']
        ];

        register_post_type($this->idPplugin, $args);

        add_action('add_meta_boxes', [$this, 'add_meta_boxes_for_request_quote']);
    }

    public function add_meta_boxes_for_request_quote()
    {
        add_meta_box('price', __('Configuración', $this->domainName), [$this, 'add_meta_boxes_for_request_quote_callback'], $this->idPplugin);
    }

    public function add_meta_boxes_for_request_quote_callback($post)
    {
        $txt_name = get_post_meta($post->ID, 'txt_mtbx_request_quote_name', true);
        $txt_description = get_post_meta($post->ID, 'txt_mtbx_request_quote_description', true);
        $txt_price = get_post_meta($post->ID, 'txt_mtbx_request_quote_price', true);
?>
        <table class="form-table">
            <tr>
                <th scope="row">
                    <label for="txt-mtbx-request-quote-name"><?php echo  __('Nombre:', $this->domainName) ?></label>
                </th>

                <td>
                    <input type="text" value="<?php echo $txt_name; ?>" id="txt-mtbx-request-quote-name" name="txt_mtbx_request_quote_name" size="30" required>
                </td>
            </tr>

            <tr>
                <th scope="row">
                    <label for="txt-mtbx-request-quote-description"><?php echo  __('Descripción:', $this->domainName) ?></label>
                </th>

                <td>
                    <input type="text" value="<?php echo $txt_description; ?>" id="txt-mtbx-request-quote-description" name="txt_mtbx_request_quote_description" size="60" required>
                </td>
            </tr>

            <tr>
                <th scope="row">
                    <label for="txt-mtbx-request-quote-price"><?php echo  __('Precio:', $this->domainName) ?></label>
                </th>

                <td>
                    <input type="number" value="<?php echo $txt_price; ?>" min="0" step=".01" id="txt-mtbx-request-quote-price" name="txt_mtbx_request_quote_price" size="5" required>
                </td>
            </tr>
        </table>
    <?php

    }


    public function save_custom_post_request_quote($post_id, $post)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;

        if ($post->post_type == $this->idPplugin) {

            // unhook this function so it doesn't loop infinitely
            remove_action('save_post', [$this, 'save_custom_post_request_quote']);

            if (isset($_POST['txt_mtbx_request_quote_name'])) {
                update_post_meta($post_id, 'txt_mtbx_request_quote_name', esc_attr($_POST['txt_mtbx_request_quote_name']));
                // update the post, which calls save_post again
                wp_update_post([
                    'ID' => $post_id,
                    'post_title' => esc_attr($_POST['txt_mtbx_request_quote_name'])
                ]);
            }

            if (isset($_POST['txt_mtbx_request_quote_description'])) {
                update_post_meta($post_id, 'txt_mtbx_request_quote_description', esc_attr($_POST['txt_mtbx_request_quote_description']));
            }

            if (isset($_POST['txt_mtbx_request_quote_price'])) {
                update_post_meta($post_id, 'txt_mtbx_request_quote_price', esc_attr($_POST['txt_mtbx_request_quote_price']));
            }

            add_action('save_post', [$this, 'save_custom_post_request_quote']);
        }
    }


    public function add_column_in_achieve_custom_post_request_quote($columns)
    {
        unset($columns['date']);
        $columns['description'] = __('Descripción', $this->domainName);
        $columns['price'] = __('Precio', $this->domainName);
        $columns['date'] = __('Fecha', $this->domainName);
        return $columns;
    }

    public function add_data_to_column_in_achieve_custom_post_request_quote($column, $post_id)
    {
        switch ($column) {
            case 'description':
                $description = get_post_meta($post_id, 'txt_mtbx_request_quote_description', true);
                echo $description;
                break;
            case 'price':
                $price = get_post_meta($post_id, 'txt_mtbx_request_quote_price', true);
                echo '$' . $price;
                break;
        }
    }


    public function add_submenu_page_post_request_quote()
    {
        $hookname = add_submenu_page(
            'edit.php?post_type=' . $this->idPplugin,
            __('Configuración', $this->domainName),
            __('Configuración', $this->domainName),
            'manage_options',
            'jl-request-quote-config',
            [$this, 'show_page_for_config_request_quote_post_type']
        );
        add_action('admin_enqueue_scripts', [$this, 'add_assets_admin_request_post_quote']);
        add_action('load-' . $hookname, [$this, 'process_page_request_post_quote']);
    }

    public function show_page_for_config_request_quote_post_type()
    {
        if (!current_user_can('manage_options')) return false;

        register_setting($this->idPplugin, $this->domainName);

        $notificate_customer_for_jl_request_quote = get_option('notificate_customer_for_jl_request_quote');

        $notificate_customer_for_jl_request_quote_body = get_option('notificate_customer_for_jl_request_quote_body');

        $notificate_admin_for_jl_request_quote = get_option('notificate_admin_for_jl_request_quote');

        $schedule_customer_for_jl_request_quote = get_option('schedule_customer_for_jl_request_quote');

        $schedule_attention_customer_for_jl_request_quote = get_option('schedule_attention_customer_for_jl_request_quote');

        $schedule_opening_hour_start_customer_for_jl_request_quote = get_option('schedule_opening_hour_start_customer_for_jl_request_quote');

        $schedule_opening_hour_end_customer_for_jl_request_quote = get_option('schedule_opening_hour_end_customer_for_jl_request_quote');
    ?>
        <div class="wrap">
            <h1><?php echo esc_html(get_admin_page_title()) ?></h1>

            <form action="<?php menu_page_url($this->idPplugin); ?>" method="POST" enctype="multipart/form-data">


                <h2>Notificaciones</h2>

                <table class="form-table" role="presentation" width="100%">
                    <tr>
                        <th scope="row" width="30%">
                            <label for="notificate_customer_for_jl_request_quote">Enviar por correo cotizaciones al cliente</label>
                        </th>
                        <td scope="row" width="5%">
                            <input type="checkbox" name="notificate_customer_for_jl_request_quote" id="notificate_customer_for_jl_request_quote" <?php if ($notificate_customer_for_jl_request_quote == 'true') : ?> checked <?php endif; ?>>
                        </td>
                        <td width="65%">Aparecerá el formulario de envio de cotizaciones por correo</td>
                    </tr>

                    <tr>
                        <th scope="row" width="30%" colspan="3">
                            <label for="notificate_customer_for_jl_request_quote_body">Cuerpo del correo electrónico</label>
                        </th>
                    </tr>
                    <tr>
                        <td scope="row" colspan="3" width="100%">
                            <textarea style="width:90%" name="notificate_customer_for_jl_request_quote_body" id="notificate_customer_for_jl_request_quote_body" <?php if ($notificate_customer_for_jl_request_quote == 'false'  || !$notificate_customer_for_jl_request_quote) : ?> disabled <?php endif; ?> required><?php echo $notificate_customer_for_jl_request_quote_body; ?></textarea>
                        </td>
                    </tr>

                    <tr>
                        <th scope="row" width="30%">
                            <label for="notificate_admin_for_jl_request_quote">Notificar a administrador</label>
                        </th>
                        <td scope="row" width="5%">
                            <input type="checkbox" name="notificate_admin_for_jl_request_quote" id="notificate_admin_for_jl_request_quote" <?php if ($notificate_customer_for_jl_request_quote == 'false' || !$notificate_customer_for_jl_request_quote) : ?> disabled <?php endif; ?> <?php if ($notificate_admin_for_jl_request_quote == 'true') : ?> checked <?php endif; ?>>
                        </td>
                        <td width="65%">Notificará al administrador cada vez que se envie una cotización por correo</td>
                    </tr>
                </table>

                <h2>Agendamiento</h2>
                <table class="form-table" role="presentation" width="100%">
                    <tr>
                        <th>
                            <label for="schedule_customer_for_jl_request_quote">Agendar citas con los clienes</label>
                        </th>
                        <td>
                            <input type="checkbox" name="schedule_customer_for_jl_request_quote" id="schedule_customer_for_jl_request_quote" <?php if ($schedule_customer_for_jl_request_quote == 'true') : ?> checked <?php endif; ?>>
                        </td>
                        <td>Aparecerá el formulario de agendamiento de citas con clientes</td>
                    </tr>

                    <tr>
                        <th scope="row">
                            <label for="schedule_attention_customer_for_jl_request_quote">Horario de atención</label>
                        </th>
                        <td scope="row">
                            <select name="schedule_attention_customer_for_jl_request_quote" id="schedule_attention_customer_for_jl_request_quote" <?php if ($schedule_customer_for_jl_request_quote == 'false'  || !$schedule_customer_for_jl_request_quote) : ?> disabled <?php endif; ?> required>
                                <option value="">--Seleccione--</option>
                                <option value="lun-vie" <?php if($schedule_attention_customer_for_jl_request_quote == 'lun-vie') { echo 'selected'; } ?>>Lunes a Viernes</option>
                                <option value="lun-sab" <?php if($schedule_attention_customer_for_jl_request_quote == 'lun-sab') { echo 'selected'; } ?>>Lunes a Sábado</option>
                                <option value="lun-dom" <?php if($schedule_attention_customer_for_jl_request_quote == 'lun-dom') { echo 'selected'; } ?>>Lunes a Domingo</option>
                            </select>
                        </td>

                        <td scope="row">
                            <label for="schedule_opening_hour_start_customer_for_jl_request_quote">Hora Apertura</label>
                            <select name="schedule_opening_hour_start_customer_for_jl_request_quote" id="schedule_opening_hour_start_customer_for_jl_request_quote" required <?php if ($schedule_customer_for_jl_request_quote == 'false'  || !$schedule_customer_for_jl_request_quote) : ?> disabled <?php endif; ?>>
                                <?php for ($i = 0; $i < 25; $i++) : ?>
                                    <?php 
                                        $selected = '';
                                        $compared = $i < 10 ? '0'.$i .":00" : $i .":00";
                                        if ($schedule_opening_hour_start_customer_for_jl_request_quote == $compared) {
                                            $selected = 'selected';
                                        }    
                                    ?>
                                    <option value="<?php if ($i < 10) {
                                                        echo '0';
                                                    } else {
                                                        '';
                                                    } ?><?php echo ($i) ?>:00" <?php echo $selected; ?>><?php if ($i < 10) {
                                                                                                                            echo '0';
                                                                                                                        } else {
                                                                                                                            '';
                                                                                                                        } ?><?php echo ($i) ?>:00</option>
                                <?php endfor; ?>
                            </select>
                            <label for="schedule_opening_hour_end_customer_for_jl_request_quote">Hora Cierre</label>
                            <select name="schedule_opening_hour_end_customer_for_jl_request_quote" id="schedule_opening_hour_end_customer_for_jl_request_quote" required <?php if ($schedule_customer_for_jl_request_quote == 'false'  || !$schedule_customer_for_jl_request_quote) : ?> disabled <?php endif; ?>>
                                <?php for ($i = 0; $i < 25; $i++) : ?>

                                    <?php 
                                        $selected = '';
                                        $compared = $i < 10 ? '0'.$i .":00" : $i .":00";
                                        if ($schedule_opening_hour_end_customer_for_jl_request_quote == $compared) {
                                            $selected = 'selected';
                                        }    
                                    ?>


                                    <option value="<?php if ($i < 10) {
                                                        echo '0';
                                                    } else {
                                                        '';
                                                    } ?><?php echo ($i) ?>:00" <?php echo $selected; ?> ><?php if ($i < 10) {
                                                                                                                            echo '0';
                                                                                                                        } else {
                                                                                                                            '';
                                                                                                                        } ?><?php echo ($i) ?>:00</option>
                                <?php endfor; ?>
                            </select>
                        </td>

                    </tr>

                </table>

                <!-- <h2>Datos</h2>
                <p>Importa los productos de forma masiva, Si encuentra productos con el mismo nombre, se actualizara y los que no se insertaran como nuevos</p>
                <p>Puedes descargar la plantilla de productos desde <a href="<?php echo $pluginDir;  ?>assets/template.csv" download="">aquí</a> </p>

                <table class="form-table" role="presentation">
                    <tr>
                        <th scope="row">
                            <label for="import">Importar productos</label>
                        </th>
                        <td>
                            <input type="file" name="products_for_jl_request_quote" id="import" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        </td>
                    </tr>
                </table> -->
                <?php

                submit_button('Guardar Configuración'); ?>
            </form>
        </div>
<?php
    }



    public function process_page_request_post_quote()
    {

        if ('POST' == $_SERVER['REQUEST_METHOD']) {


            if (isset($_POST['notificate_customer_for_jl_request_quote'])  && $_POST['notificate_customer_for_jl_request_quote'] == 'on') {
                update_option('notificate_customer_for_jl_request_quote', 'true');
            } else {
                update_option('notificate_customer_for_jl_request_quote', 'false');
            }

            if (isset($_POST['notificate_customer_for_jl_request_quote_body'])  && $_POST['notificate_customer_for_jl_request_quote_body'] != '') {
                update_option('notificate_customer_for_jl_request_quote_body', $_POST['notificate_customer_for_jl_request_quote_body']);
            } else {
                update_option('notificate_customer_for_jl_request_quote_body', '');
            }


            if (isset($_POST['notificate_admin_for_jl_request_quote'])  && $_POST['notificate_admin_for_jl_request_quote'] == 'on') {
                update_option('notificate_admin_for_jl_request_quote', 'true');
            } else {
                update_option('notificate_admin_for_jl_request_quote', 'false');
            }


            //Schedule
            if (isset($_POST['schedule_customer_for_jl_request_quote'])  && $_POST['schedule_customer_for_jl_request_quote'] == 'on') {
                update_option('schedule_customer_for_jl_request_quote', 'true');
            } else {
                update_option('schedule_customer_for_jl_request_quote', 'false');
            }



            if (isset($_POST['schedule_attention_customer_for_jl_request_quote'])  && $_POST['schedule_attention_customer_for_jl_request_quote'] != '') {
                update_option('schedule_attention_customer_for_jl_request_quote', $_POST['schedule_attention_customer_for_jl_request_quote']);
            } else {
                update_option('schedule_attention_customer_for_jl_request_quote', 'false');
            }

            if (isset($_POST['schedule_opening_hour_start_customer_for_jl_request_quote'])  && $_POST['schedule_opening_hour_start_customer_for_jl_request_quote'] != '') {
                update_option('schedule_opening_hour_start_customer_for_jl_request_quote', $_POST['schedule_opening_hour_start_customer_for_jl_request_quote']);
            } else {
                update_option('schedule_opening_hour_start_customer_for_jl_request_quote', 'false');
            }

            if (isset($_POST['schedule_opening_hour_end_customer_for_jl_request_quote'])  && $_POST['schedule_opening_hour_end_customer_for_jl_request_quote'] != '') {
                update_option('schedule_opening_hour_end_customer_for_jl_request_quote', $_POST['schedule_opening_hour_end_customer_for_jl_request_quote']);
            } else {
                update_option('schedule_opening_hour_end_customer_for_jl_request_quote', 'false');
            }
            //die();
        }



        // if (isset($_POST['submit']) && isset($_FILES['products_for_jl_request_quote'])) {
        //     $file = $_FILES['products_for_jl_request_quote'];

        //     $fileName = explode('.', $file['name']);
        //     if ($fileName[1] == 'csv') {
        //         $handle = fopen($file['tmp_name'], 'r');

        //         while ($data = fgetcsv($handle)) {
        //             if (
        //                 $data[0] != 'name' ||
        //                 $data[1] != 'description' ||
        //                 $data[2] != 'price' ||
        //                 $data[3] != 'categories'
        //             ) {
        //                 $fName = $data[0];
        //                 $fDescr = $data[1];
        //                 $fPrice = $data[2];
        //                 $fCategories = $data[3];

        //                 $this->saveOrUpdateJlRequestProducts($fName, $fDescr, $fPrice, $fCategories);
        //             }
        //         }



        //         fclose($handle);
        //     }
        // }
    }


    public function saveOrUpdateJlRequestProducts($fName, $fDescr, $fPrice, $fCategories)
    {
        $args = [
            'post_type' => $this->idPplugin,
            'posts_per_page' => '1',
            'meta_query' => [
                [
                    'key' => 'txt_mtbx_request_quote_name',
                    'value' => $fName,
                    'compare' => '='
                ],
            ]
        ];
        $query = new WP_Query($args);
        while ($query->have_posts()) {
            $postID = $query->post->ID;
            update_post_meta($postID, 'txt_mtbx_request_quote_name', esc_attr($fName));
            update_post_meta($postID, 'txt_mtbx_request_quote_description', esc_attr($fDescr));
            update_post_meta($postID, 'txt_mtbx_request_quote_price', esc_attr($fPrice));
        }


        return true;
    }

    public function hidde_button_preview_request_post_page()
    {
        global $post_type;

        if ($post_type == $this->idPplugin) {
            echo '<style>
            #post-preview{
                display:none !important;
            }               
            } 
          </style>';
        }
    }

    public function remove_row_action_on_preview_request_post($actions)
    {
        global $post_type;
        if ($post_type == $this->idPplugin) {
            unset($actions['view']);
        }

        return $actions;
    }


    public function add_assets_admin_request_post_quote()
    {
        wp_register_script('jl-request-admin-app.js', plugin_dir_url(__FILE__) . '../assets/js/jl-request-admin-app.js', true);
        wp_enqueue_script('jl-request-admin-app.js');
    }
}
