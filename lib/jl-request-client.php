<?php

require_once 'jl-base-request.php';

class JlRequestClient extends JlBaseRequest
{
    public function __construct()
    {
        add_shortcode('jl_request_quote', [$this, 'register_short_jl_request_short_code']);


        add_action('admin_post_' . $this->idPplugin,  [$this, 'handle_form_send_email_request']);
        add_action('admin_post_nopriv_' . $this->idPplugin,  [$this, 'handle_form_send_email_request']);
    }

    public function register_short_jl_request_short_code($atts)
    {
        $this->register_assets();

        $contanier_name = str_replace('_', '-', $this->idPplugin);

?>

        <div id="<?php echo $contanier_name;   ?>-container">

            <?php
            $this->load_indications($contanier_name);
            $this->load_item_selector($contanier_name);
            $this->load_table_prices($contanier_name);
            if (get_option('notificate_customer_for_jl_request_quote') == 'true') {
                $this->load_send_email_form($contanier_name);
            }
            ?>



        </div>

    <?php
    }

    public function register_assets()
    {
        wp_register_script('choices.js', plugin_dir_url(__FILE__) . '../assets/js/choices.min.js', true);
        wp_register_style('choices.css', plugin_dir_url(__FILE__) . '../assets/css/base-choices.min.css');
        wp_register_script('jl-request-cilent-app.js', plugin_dir_url(__FILE__) . '../assets/js/jl-request-cilent-app.js', true);
        wp_register_style('jl-request-cilent-app.css', plugin_dir_url(__FILE__) . '../assets/css/jl-request-cilent-app.css');


        wp_enqueue_script('choices.js');
        wp_enqueue_script('jl-request-cilent-app.js');
        wp_enqueue_style('choices.css');
        wp_enqueue_style('jl-request-cilent-app.css');
    }

    private function load_indications($contanier_name)
    {
    ?>
        <section id="<?php echo $contanier_name ?>-indications">
            <p><strong>Indicaciones:</strong></p>
            <ol>
                <li>Buscar y seleccionar el examen que desea cotizar en el menu despegable</li>
                <li>Presionar sobre el botón '+' para agregar a la lista de items a cotizar</li>
                <li>Visualizará en el lado derecho el precio total de lo que desea cotizar</li>
            </ol>
        </section>
    <?php
    }

    private function load_item_selector($container_name)
    {
        $paramQuery = [
            'post_type' => $this->idPplugin,
            'post_status' => 'publish'
        ];

        $query = new WP_Query($paramQuery);
    ?>
        <section id="<?php echo $container_name; ?>-select-container">
            <label for="<?php echo $container_name; ?>-select-form">Items</label>
            <select name="<?php echo $container_name; ?>-select-form" id="<?php echo $container_name; ?>-select-form" placeholder="Seleccione un Item">
                <option value="">Seleccione un item</option>

                <?php
                if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                        $price = get_post_meta(get_the_ID(), 'txt_mtbx_request_quote_price', true);
                ?>
                        <option value="<?php echo the_title() . 'separator' . $price;  ?>"><?php the_title(); ?> - $ <?php echo $price; ?></option>
                <?php endwhile;
                endif; ?>
            </select>
        </section>
    <?php wp_reset_query();
    }


    private function load_table_prices($contanier_name)
    {
    ?>
        <table id="<?php echo $contanier_name ?>-table">
            <thead>
                <tr>
                    <th></th>
                    <th>Item</th>
                    <th>Precio</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                        Total:
                    </td>
                    <td id="<?php echo $contanier_name ?>-total">

                    </td>
                </tr>
            </tfoot>
        </table>
    <?php
    }

    private function load_send_email_form($container_name)
    {
    ?>
        <section id="<?php echo $container_name; ?>-section-form">

            <button id="<?php echo $container_name; ?>-send-by-email" type="button" disabled>Enviar por correo</button>
            <form action="<?php echo esc_url(admin_url('admin-post.php')); ?>" method="post" id="request-quote-form" class="<?php echo $container_name ?>-form-hide">
                <p>Envie las cotizaciones por correo electrónico</p>
                <label for="<?php echo $container_name; ?>-email-field">Nombre</label>
                <input type="text" name="<?php echo str_replace("-", "_", $container_name);  ?>_name_field" id="<?php echo $container_name ?>-name-field" required autofocus>

                <label for="<?php echo $container_name; ?>-phone-field">Teléfono</label>
                <input type="text" name="<?php echo str_replace("-", "_", $container_name);  ?>_phone_field" id="<?php echo $container_name ?>-phone-field" required>
                
                <label for="<?php echo $container_name; ?>-email-field">Correo electrónico</label>
                <input type="email" name="<?php echo str_replace("-", "_", $container_name);  ?>_email_field" id="<?php echo $container_name ?>-email-field">
                
                <input type="hidden" name="action" value="<?php echo str_replace("-", "_", $container_name); ?>">
                
                <input type="checkbox" name="<?php echo str_replace("-", "_", $container_name);  ?>_schedule_field" id="<?php echo $container_name; ?>-schedule-field">
                <label for="<?php echo $container_name; ?>-schedule-field">¿Desea Ser contactado para agendar una cita?</label>
                
                <button type="submit">Enviar</button>
            </form>
        </section>

        <?php if (isset($_GET['success'])) :  ?>
            <div id="<?php echo $container_name; ?>-section-form-message">
                <?php if ($_GET['success'] == true) :  ?>
                    <p>La cotización ha sido enviada satisfactoriamente. Revisa tu correo por favor</p>
                <?php else : ?>
                    <p>La cotización no pudo ser enviada</p>
                <?php endif; ?>
            </div>
        <?php endif; ?>
<?php
    }


    public function handle_form_send_email_request()
    {   

        $location = wp_get_referer();
        
        if (isset($_POST['request_quote_email_field']) && $this->validate_email($_POST['request_quote_email_field'])) {
            if (isset($_POST['request_quote_hidden_item'])) {
      
                $form_url = home_url('/');
                $customer_email = $_POST['request_quote_email_field'];
                //add_filter('wp_mail_content_type', [$this, 'content_type_html']);

                $send_email_customer  = $this->send_email_customer($customer_email, $_POST['request_quote_hidden_item']);

                if (get_option('notificate_admin_for_jl_request_quote') == 'true') {
                    $this->send_email_admin($customer_email, $_POST['request_quote_hidden_item']);
                }

                if ($send_email_customer) {
                    wp_safe_redirect($location . '?success=true');
                } else {
                    wp_safe_redirect($location . '?success=false');
                }
            } else {
                wp_safe_redirect($location . '?invalid=true&noitems=true');
            }
        } else {
            wp_safe_redirect($location . '?invalid=true&noemail=true');
        }
        exit();
    }
}
